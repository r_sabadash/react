const todos = [
    {
        id: 1,
        title: 'Learn JS',
        completed: true
    },
    {
        id: 2,
        title: 'Learn React',
        completed: false
    },
    {
        id: 3,
        title: 'Develop Applicaton',
        completed: false
    }
];

export default todos;